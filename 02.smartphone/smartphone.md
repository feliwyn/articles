---
title: Expérience smartphone
blog_url: smartphone
body_classes: header-image fullwidth

sitemap:
    changefreq: monthly
    priority: 1.03

content:
    items: @self.children
    order:
        by: date
        dir: desc
    limit: 5
    pagination: true

feed:
    description: Fourre-tout Libre
    limit: 10

pagination: true
---

# <i class="fa fa-mobile"></i>  L'expérience smartphone
## Je me sépare de mon smartphone pour quelque temps
