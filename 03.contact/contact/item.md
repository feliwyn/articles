---
title: Me contacter
date: 27-12-2013
taxonomy:
	category: contact
	tag: [zilkos]
summary:
	enabled: false
---

La plus simple des manières étant encore de m’envoyer un mail à l’adresse zilkos [a] unixmail [point] fr. Si toutefois vous chiffrez vos mails, ma clef publique est disponible ci-après, si vous ne chiffrez pas, je vous encourage à le faire.



>>>>>> Clef publique: [D9034A4E](https://pgp.mit.edu/pks/lookup?op=vindex&search=0x4E914CE5D9034A4E)

