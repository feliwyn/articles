---
title: Finch, un Pidgin en console !
date: 02-08-2013 12:00
taxonomy:
	category: blog
	tag: [Linux, cli]
---

Pidgin, le célèbre client de messagerie supportant énormément de protocoles est utilisé par beaucoup de personnes, que ce soit sous GNU/Linux ou Windows. Oui, mais saviez-vous qu'un _Pidgin like_ en console existait ? Il se nomme Finch, voyons ça !

===

Pidgin c’est un logiciel libre de messagerie instantanée multi-plateforme et multiprotocole. Il supporte environ 16 protocoles, allant de XMPP jusqu’à IRC/ICQ. Sa version graphique est très bien faite, simple et efficace.

Le but de cet article est donc de vous présenter son alternative en console: Finch !

Installez-le, il est disponible dans les dépôts, rien de bien compliqué.

### Configurer un compte

Déjà, si vous êtes utilisateur de Pidgin, vous n’avez rien à faire vu que les deux logiciels se partagent les informations des comptes (~/.purple/accounts.xml). Donc vous n’avez plus qu’à le lancer.

>>>>> Pidgin et Finch s’appuient sur Libpurple (ex libgaim) originellement écrite en C.

Pour les autres, la configuration de compte doit arriver au lancement de l’application, si ce n’est pas le cas, faites un `Alt + a` puis `Entrée` (la première entrée surlignée est _Comptes_).

Pour la suite, remplissez les champs et naviguez avec `Tab` et utilisez les flèches pour dérouler les menus.

### Navigation

Le plus impressionnant dans ce logiciel, c’est bien qu’on soit en console, on peut déplacer ou redimensionner une fenêtre et même gérer des espaces de travail (workspace).

Bien évidemment, Finch ne répond qu’au clavier, ce qui est un peu le principe d’un logiciel en console (c’est pas complètement vrai, mais bon). Voyons les raccourcis clavier:

* `Alt + a` : (action) Affiche la fenêtre d’actions, comportant la gestion des comptes, les préférences et autres réglages (salons, sons, états, plugins, etc…).
* `Alt + w` : (window) Affiche la liste des fenêtres ouvertes, mettez en surbrillance la fenêtre désirée et appuyez sur Entrée pour avoir le focus dans ladite fenêtre.
* `Alt + p` : (previous) Va à la fenêtre précédente.
* `Alt + n` : (next) Va à la fenêtre suivante.
* `Alt + c`: (close) Ferme la fenêtre ayant le focus.
* `Alt + m` : (move) Déplace une fenêtre. Utilisez les flèches pour la bouger, puis Entrée pour valider le nouvel emplacement.
* `Alt + r` : (redim) Redimensionne une fenêtre. Utilisez les flèches puis Entrée pour valider la nouvelle dimension.
* `Alt + .` : Déplace la fenêtre ayant le focus vers la droite.
* `Alt + ,` : Déplace la fenêtre ayant le focus vers la gauche.
* `Ctrl + o` : Affiche le menu de la fenêtre si elle en a un (F10 fait la même chose).
* `Ctrl + x` : Affiche le menu Widget (F11 fait la même chose).

En ce qui concerne les espaces de travail:

* `F9` : Crée un nouvel espace de travail
* `Alt + >` : Va à l’espace de travail suivant
* `Alt + <` : Va à l’espace de travail précédent
* `Alt + s` : Affiche la liste des espaces de travail
* La plupart de ces raccourcis claviers sont donc bien pensés et suivent une certaine logique qui fait qu’on les retient très rapidement et par conséquent on maitrise relativement vite le logiciel.

### Pour les drogués de la souris

A la base que je ne voulais pas écrire cette partie. Mais bon, autant tout présenter. Donc pour les allergiques du clavier, Finch peut supporter la souris.

Il faut créer le fichier `~/.gntrc` (donc `/home/VOUS/.gntrc`) et y insérer le contenu suivant:

```bash
[general]
mouse = 1
[Finch]
mouse = 1
```


Bon t'chat ! ;)
