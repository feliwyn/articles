---
title: Elementary OS, quelques astuces
date: 13-11-2013 12:00
taxonomy:
	category: blog
	tag: [Linux]
---

Voyons voir comment améliorer votre distribution GNU/Linux Elementary OS à travers quelques astuces et conseils !

===

ElementaryOS (abrégé EOS par la suite), une distribution GNU/Linux qui mérite qu’on s’y attarde un peu. Je l’utilise depuis quelques temps déjà et j’en suis très satisfait. Ce petit article présente quelques astuces et personnalisations de l’OS. Si toutefois vous ne savez pas ce que c’est, l’ami [@maxauvy](http://open-freax.fr/) avait fait un test sur [la beta 2](http://open-freax.fr/test-elementaryos-0-2/) qui, à l’époque, était déjà bluffante.

Nous verrons comment ajouter le bouton Réduire sur les fenêtres, nous verrons aussi un paquet merveilleux nommé `elementary-tweaks` ainsi que comment créer un groupe d’application dans le dock (Plank).

### Un bouton pour réduire les fenêtre

Pantheon, l’environnement de bureau d’EOS est par défaut épuré. Tellement épuré que les fenêtres possèdent deux boutons, un pour la fermer et un pour la maximiser, respectivement à gauche et à droite.

Comment on réduit la fenêtre ? Via `Ctrl + Alt + 0` par exemple, mais c’est pas toujours pratique pour les personnes utilisant la souris (honte à vous). Au pire, allez chercher votre application dans le dock pour la réduire, pas franchement très pratique non plus.

Pour faire apparaitre le bouton réduire, on va installer `dconf-tools` via la console ou la logithèque. Il permet de modifier les variables de la configuration de l’environnement. Lancez le.

Attention, ne faites pas n’importe quoi ! Dans l’arborescence à gauche de la fenêtre, rendez vous dans:

`org > pantheon > desktop > gala > appearance`

Une fois ici, repérez la ligne `button-layout`, il y a 3 lignes dont deux avec une case à cocher, donc difficile de vous tromper.

Cette ligne permet d’indiquer un _layout_ de bouton sur le haut des fenêtres, par _layout_ entendez positionnement (quel bouton va où).

Par défaut cette ligne contient `close:maximise`. Les deux points indiquent le séparateur. Ce qu’il y a à gauche des deux points, je le colle le plus à gauche de la fenêtre, idem pour la partie de droite (pour schématiser).

Ainsi, si vous souhaitez que le bouton Réduire se trouve à droite du bouton Fermer, le tout à gauche de la fenêtre vous écrirez ceci: `close,minimize;maximize` ce qui donne (attention super schéma de la mort):

 `[ x ↓            <>]`

Si vous voulez le positionnez à droite, avant le bouton d’agrandissement de la fenêtre vous écriez: `close:minimize,maximise`

Ce qui donne: `[x            ↓<>]`

Notez que le résultat est immédiat une fois que vous cliquez hors du champs `button-layout`. Si toutefois vous êtes perdu, la valeur par défaut est `close:maximize`.

### Plus de paramètres !

Pour des paramètres en folie, on va installer `elementary-tweaks`, une application qui va vous permettre d’avoir la main sur beaucoup plus de paramètres que par défaut, le tout très bien intégré et sans passer par dconf-tools.

On ouvre un terminal (parce que c’est la vie) et on y tape:

```bash
sudo apt-add-repository ppa:versable/elementary-update
```

ce qui a pour but de rajouter le PPA (Personnal Package Archives) maintenu par une équipe de passionnés d’EOS, merci à eux en passant. Qui dit nouveau PPA, dit nouveaux paquets disponibles donc on met à jour le cache des paquets via un:

```bash
sudo apt-get update
```


Puis on installe tranquillement nos deux paquets elementary-tweaks et wingpanel-slim:

```bash
sudo apt-get install elementary-tweaks wingpanel-slim
```

Et hop, c’est fini !

Allez donc dans les paramètres système pour y trouver une nouvelle icône nommée Tweaks. A partir de là, vous avez énormément de possibilités de personnalisations et de configurations pour à peu près tout !

### Plank, un bouton pour les dominer tous !


Plank, ce dock merveilleux. Prenons un cas pratique pour illustrer la manipulation.
Vous avez un netbook, et votre dock est blindé de raccourcis vers diverses applications. Comme vous avec une vie sociale énorme vous disposez comme raccourcis de:

* Pidgin, pour parler avec les potes.
* Polly, pour troller avec les potes sur Twitter.
* Un client mail, pour discuter avec les arriérés qui ne connaissent pas Jabber.
* XChat pour draguer des donzelles qui s’avèrent en fait être des hommes avec un fort embonpoint, mais avec un pseudo attirant.
Tout ceci prend 4 emplacements sur votre dock. On va les transformer en un seul emplacement qui contiendra les 4 applications, qu’on pourra lancer indépendamment les unes des autres.

En premier lieu, allez dans votre dossier personnel (`/home/vous`) et créez un dossier nommé Plank. Ce dossier contiendra nos _groupes d’applications_. Allez dans le dossier Plank et créer un dossier, portant le nom de votre groupe d’applications, dans notre cas, ça sera **Social**. Pourquoi donner un nom aussi évocateur ? Parce que les autres applications du dock possèdent un tooltip. Un tooltip c’est une infobulle qui apparait quand on passe la souris sur une icône du dock. Dans notre cas, le tooltip de notre groupe d’applications est égal au nom du dossier utilisé pour créer ce groupe, donc **Social**.

Résumons, on a un dossier `/home/vous/Plank` qui contient Social, ce qui revient à dire qu’on a un groupe d’applications nommé Social. Bien, on va maintenant y mettre les applications énoncées plus haut.

Ouvrez Files, votre gestionnaire de fichier et allez dans `/usr/share/applications`. Joie, on y retrouve toutes les icônes des applications graphiques. Sélectionnez les applications que vous voulez faire apparaitre dans votre groupe, donc dans notre cas Pidgin, Polly, le client mail et XChat, copiez les et collez les dans `/home/vous/Plank/Social/`.

Notre dossier Social contient maintenant nos lanceurs d’applications. Il vous suffit de faire glisser le dossier Social sur le dock pour qu’un raccourci unique soit créé. Une icône noire avec 4 minis icônes dedans apparait, il suffit de cliquer dessus pour choisir quel logiciel lancer !

Comme je suis mignon, je vous donne la version console de ces manipulations (oui, j’suis comme ça), on part du dossier utilisateur (donc `/home/vous/`):

```bash
$ mkdir -p Plank/Social
$ cd /usr/share/applications/
$ cp pidgin.desktop claws-mail.desktop xchat.desktop polly.desktop /home/VOUS/Plank/Social/
```

Il ne vous reste plus qu’à lancer Files et faire glisser le dossier Social dans le dock (oui, je vous fais grâce de l’écriture du `Social.dockitem` dans `/home/vous/.config/pank/dock1/launchers`).

Enjoy ! 