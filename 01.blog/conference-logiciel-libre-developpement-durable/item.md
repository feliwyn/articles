---
title: Conférence: Les Logiciels Libres, moteurs de l’innovation et du Développement Durable
date: 08-03-2015 16:00
taxonomy:
	category: blog
	tag: [conference]
---

L'ami Maxime Auvy vient nous gratifier de ses connaissances sur les Logiciels Libres en tant que moteur pour l'innovation et le développement durable dans une conférence à l'Université de Technologie de Belfort-Montbéliard !

===

Comme annoncé sur [son blog](http://open-freax.fr/conf-logiciels-libres-developpement-durable/) et via [Twitter](https://twitter.com/maxauvy/status/573843800071782400), Maxime Auvy va venir à l'Université de Technologie de Belfort Montbéliard pour nous parler sur le thème des Logiciels Libres comme moteurs de l'innovation et du Développement Durable.

#### Maxime qui ?
Fortement calé dans le domaine, Maxime Auvy est Ingénieur de Recherche au Centre de Recherches et d’Études Interdisciplinaires sur le Développement Durable ([CREIDD](http://creidd.utt.fr/fr/index.html)), un des laboratoires de l’Institut Charles Delaunay ([ICD](http://icd.utt.fr/fr/index.html)), à l’Université de Technologie de Troyes ([UTT](http://www.utt.fr/fr/index.html)).

Diplômé de l'UTT en tant qu'ingénieur _Materiaux: Technologie et Economie_ de la filière _Économie des Matériaux et Environnement_, il est également titulaire d'un Master IMEDD (_Ingénierie et Management de l’Environnement et du Développement Durable_).

Outre les compétences évoquées ci-dessus, il participe aussi à plusieurs projets libres, comme openLCA, ownCLoud, TextSecure, RedPhone et CyanogenMod. Il est aussi formateur C2i.

#### Où ? Quand ?

C'est avec toutes ses connaissances et une réelle passion pour ce sujet qu'il viendra tenir cette conférence à l'[UTBM](www.utbm.fr), université dont je suis moi-même allé tenir [une conférence sur l'auto-hébergement](http://unixmail.fr/blog/conference-auto-hebergement), organisée par [Fr33tux](https://fr33tux.org/).

Pour la magnifique affiche de cette conférence, je vous renvoie à l'article de Maxime [sur son blog](http://open-freax.fr/conf-logiciels-libres-developpement-durable/#more-2373). Lisez-le, vous apprendrez plein de choses !

Rendez-vous donc le **jeudi 26 mars à 18h30 en A200 à l'UTBM Belfort** pour cette conférence organisée par [LolUT](https://ae.utbm.fr/lolut/) !