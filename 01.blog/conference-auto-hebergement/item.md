---
title: Conférence: L'auto-hébergement: Pourquoi, pour qui, comment ?
date: 03-11-2014 12:00
taxonomy:
	category: blog
	tag: [conference, selfhosting]
---

Comme vous le savez, du moins pour les fidèles d’Unixmail, j’ai une passion pour l’auto-hébergement. C’est donc avec joie que j’ai proposé à Fr33tux ma maigre contribution, sous forme d’une conférence à l’Université de  [Technologie Belfort Montbéliard](http://www.utbm.fr/). Un peu de blabla libre et ouvert pour exprimer beaucoup de choses et répondre à de nombreuses questions concernant l’auto-hébergement ! Oui, même vos questions !

===

>>>>>> Les vidéos de la conférence: [Version basse qualité (800Mo / OGG)](https://fr33tux.org/data/zilkos_SD_final.ogg) [Version haute qualité (8Go / MP4)](https://fr33tux.org/data/zilkos_HD.mp4)


>>>>>> Merci à fr33tux pour l’hébergement. Attention, le son n’est pas de très bonne qualité, un grand merci à ceux qui ont participé à la mise en place de cette conférence.

Ci-après, la présentation de la conférence:

> Cette conférence aura lieu le jeudi 13 novembre à 18:30 (en A200 sur le site de Belfort) et abordera le sujet en tentant de répondre à diverses questions, telles que:
– L’auto-hébergement, c’est quoi ?
– Pourquoi vouloir s’auto-héberger ?
– Comment faire et avec quels moyens ?
De près ou de loin les logiciels libres seront abordés, ainsi que quelques autres thématiques transverses, notamment la culture hacker, les mails, l’importance de vos données privées, etc.
Zilkos est blogueur sur http://unixmail.fr et a proposé un service d’hébergement de mail / jabber / serveur de fichier / mailing listes / git / gitweb basé exclusivement sur du logiciel libre.
Vous êtes toutes et tous les bienvenu(e)s (entrée _libre_ !), que vous soyez ou non technophile ou simplement curieux/se.
En espérant vous y voir nombreux-ses !

[Lolut:](http://ae.utbm.fr/lolut/) Association étudiante de promotion de la culture libre : art, logiciel, philosophie…

[L'affiche et le sujet de la conférence sur le forum Lolut](https://ae.utbm.fr/forum2/?id_sujet=16243)
