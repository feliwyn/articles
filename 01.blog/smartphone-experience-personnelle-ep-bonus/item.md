---
title: Smartphone: L'expérience personnelle - Episode Bonus
date: 27-12-2013 16:00
taxonomy:
	category: blog
	tag: [smartphone, 3310, FOMO]
---

Un épisode bonus de l’expérience personnelle concernant les smartphones où je vous donne quelques ressources pour que vous puissiez continuer votre réflexion.

Au menu, des explications, des liens, des ressources, bref tout ce qu’il faut pour clore pour cette expérience et vous donner l’envie d’aller lire d’autres choses (plus sérieuses et pertinentes que les miennes) sur le sujet.

===

>>>>> Pour le premier épisode, [c'est par ici](http://unixmail.fr/smartphone/smartphone-experience-personnelle-ep1).
>>>>> Pour le deuxième épisode, [c'est par ici](http://unixmail.fr/smartphone/smartphone-experience-personnelle-ep2).
>>>>> Pour le troisième épisode, [c'est par ici](http://unixmail.fr/smartphone/smartphone-experience-personnelle-ep3).

## Journées mondiales sans téléphone mobile

Entendu hier à la radio, le 6, 7 et 8 février sont les journées mondiales sans téléphone mobile. Elles ont lieu chaque année depuis le 6 février 2001.

C’est un écrivain français qui est l’auteur de ce mouvement mondial, Phil Marso, qui a publié le premier roman policier autour du sujet du téléphone portable vers la fin de l’année 1999, roman magnifiquement intitulé "Tueur de portable sans mobile apparent" . Il a aussi créé sa propre société d’édition indépendante, nommée "Megacom-ik".

>>>>> La petite histoire raconte que le choix de la date de ce mouvement fut trouvé grâce à une chanson de Nino Ferrer « Gaston y a le téléfon qui son, y a person qui y répond« , la saint Gaston tombant un 6 février, cette date a été retenue.

Chaque année, cette période sans téléphone mobile est rythmée par différents thèmes. Cette année le thème est "Le langage SMS abrégé" . Sur les affiches on peut voir le slogan: "Alerte SMS: Enlèvement de la langue française ?"

Notez aussi que Phil Marso a sorti un livre électronique (format e-book)  intitulé _Adikphonia_, un mot qu’il a inventé car le mot équivalent américain _nomopobia_ (nomophobie en français) était trop près du mot _homophobie_, il avait donc peur que les gens fassent l’amalgame. Il est au prix de 2,68€ sur la plateforme Kindle d’Amazon. (Attention, les Kindles ne sont pas libres, et les livres achetés peuvent être supprimés à distance, sans votre consentement et sans préavis). La présentation du livre est la suivante:

> Maladie addictive provenant d’avancées technologiques, plus particulièrement des téléphones portables et des nouvelles générations de Smartphones. L’individu vit 24 h / 24 dans la connexion compulsive de peur d’être coupé du monde. La séparation peut être matérielle (ne peut plus toucher l’objet) ou due à une défection-technologique. Ce manque peut entraîner des troubles de nervosité, d’anxiété passagère, voire permanente. La phase aiguë de la maladie est atteinte quand le sujet est replié sur lui-même, dans un monde virtuel où l’absence de contact humain le submerge.

> La subtilité du 24/24 est de démontrer que posséder un Smartphone ne fait pas de vous un futur Adikphoniak. Ceci est bien entendu pure propagande !

> Ce livre numérique « ADIKPHONIA » est une première ébauche de nos agents 2902XX qui tente de contenir la maladie fortement contagieuse. Ce guide de survie sera susceptible de s’étoffer au fil des semaines si toutefois le 24/24 n’élimine pas notre groupe de résistance.

> Le 6, 7, 8 février 2013 tous nos agents du 2902XX montent en première ligne lors des 13ème Journées Mondiales sans téléphone portable & Smartphone. Rejoignez-nous !


## Nomophobie

Dans mes expériences, je vous parlais de FOMO, et bien la nomophobie ou adikphonia comme dit Phil Marso, c’est la phobie d’être séparé de son téléphone portable. Le mot vient du terme anglais « no mobile-phone phobia« .

Des recherches très sérieuses ont été menées sur ce sujet et indiquaient que 53% des utilisateurs de téléphone mobile s’inquiétaient quand leur téléphone était perdu, vidé de son crédit ou en panne de batterie.

Certes, mot phobie est exagéré. Il s’agirait plus d’une inquiétude grandissante avec le temps de non possession de l’appareil, même si la base du problème reste le même: ne pas posséder, et avoir peur de rater quelque chose.

Si vous maitrisez un peu l’anglais, le docteur Mario Lehenbauer-Baum a écrit un très bel article explicatif sur le sujet, intitulé [I am a nomophobic !](http://www.drlehenbauer.com/2013/08/i-am-a-nomophobic-nomophobia/)" le 28 aout dernier. Je vous conseille fortement de lire son blog, on y trouve plein de choses intéressantes  sur la psychologie positive (son domaine de prédilection semble-t-il).

Pour terminer, un papier très intéressant tiré de l’International Journal of Social Science and Humanity , intitulé _The Conceptual Model on Smart Phone Addiction among Early Childhood_ , [c’est disponible ici](http://www.ijssh.org/papers/336-A10048.pdf), en anglais et écrit par Cheol Park et Ye Rang Park.  C’est franchement très intéressant et l’anglais y est abordable, même si c’est plus centré sur l’adolescence / l’enfance.

## Conclusion

Cet épisode bonus, bien que court, marque la fin de l’expérience. En regardant le nombre de vues pour ces articles, c’est visiblement un succès. Encore une fois, je vous invite à tenter l’expérience et à vous intéresser à toutes ces choses, d’un point de vue personnel mais aussi d’un point de vue sociologique.

C’est bougrement rigolo et vous verrez les choses différemment, au final, vous comprendrez que la seule chose que vous faites, c’est vous imposer des contraintes.

Ce n’est pas parce que la suite d’article est terminée qu’on a pas le droit d’échanger sur le sujet !

Maintenant, posez votre téléphone intelligent, et profitez de la vie !
