---
title: Nomophobes anonymes et mélatonine
date: 06-03-2014 12:00
taxonomy:
	category: blog
	tag: [smartphone, nomophobie]
---
La nomophobie est un sujet déjà abordé dans l’expérience concernant les smartphones. Voyons plus en détails ce qu’est la nomophobie, les symptômes et les actions pour s’en sortir.

===

Souvenez-vous, j’avais tenté une expérience concernant les smartphones, puis fait une digression concernant la nomophobie et l’adikphonia dans un épisode bonus pour clôturer la saga.

Sauf que oui, ce sujet, je l’apprécie particulièrement. Comme on avait vu des signes qui pouvait annoncer une trop grande addiction, on va voir comment faire pour se tirer de cette addiction, d’où le titre de l’article, plus à but humoristique qu’autre chose.

Vu qu’en ce moment certains humoristes n’ont pas forcément le vent en poupe, je précise quand même que ce titre (du moins les deux premiers mots) est uniquement à but humoristique et qu’il n’a pas pour objet de se moquer de personnes se réunissant quelques fois par semaine dans des réunions pour partager leurs expériences et motivations quant à l’arrêt ou la maitrise de leur addiction.

Je rappelle que c’est un sujet grave, qui peut entrainer de lourdes conséquences pour certains (mais on va pas traiter ça dans ce sens, ou tout le monde finira dépressif avant la fin de l’article…).

### Les symptômes

Un bref rappel concernant les symptômes d’une nomophobie:

* Je regarde mon téléphone plus d’une fois par demi-heure
* Je vérifie si il est dans ma poche / mon sac très souvent 
    - Si il n’y est pas, ça va pas du tout
* Je dors avec mon téléphone allumé sur la table de nuit / sous mon oreiller
* Je sursaute comme un lapin à chaque sonnerie de notification et me rue sur mon téléphone

Bref, si votre téléphone est le prolongement de votre bras, que les réseaux sociaux sont votre seule nourriture, vous avez des soucis à vous faire. Donc c’est bien de savoir si on présente un début de nomophobie mais c’est encore mieux de savoir comment s’en débarrasser…

### Prévention

Comme de nombreuses addictions, plus c’est détecté tôt et plus vous avez de chance de vous en rétablir vite. Encore faut-il ne pas se voiler la face et accepter le problème. Bien évidemment, plus c’est détecté tôt et moins vous aurez de travail à fournir pour vous en sortir, schéma qu’on retrouve souvent dans le milieu médical pour les maladies progressives (cancers, addictions, etc…).

Alors, quelques techniques pour les nomophobes légers, conscients de leur dépendance et voulant s’en sortir, ou a minima, faire baisser le niveau de dépendance:

#### Définissez une plage horaire

Vous devez auto-réguler vos consommations téléphoniques / de nouvelles technologies. Posez des plages horaires fixes réalisables (et en accord avec votre vie professionnelle, vos occupations) durant lesquelles vous éteignez votre téléphone / ordinateur. Essayez de restreindre cette plage au fur et à mesure du temps, mais toujours très progressivement. Diminuez par tranches de 15 minutes chaque semaine jusqu’à arriver à des horaires corrects d’utilisation non abusives (le but n’est pas non plus de vous couper de tous ces objets).

Imposez-vous directement d’éteindre vos appareils de communication avant de dormir. Hors de question d’avoir un téléphone allumé la nuit à côté de vous, une tablette ou autre. Ne serait-ce que pour les diverses ondes qui transitent par les appareils (et donc par vous).

#### Pas de téléphone au coucher et au lever

À coupler avec la règle précédente pour « être sûr ». Ne consultez pas votre téléphone avant de vous coucher, ça va vous stresser, et vous aurez beaucoup de mal à vous en séparer, ce qui va rallonger le temps de se coucher, et donc diminuer votre total de sommeil. Idem pour les ordinateurs, évitez d’y passer 3h d’affilée avant le coucher, vous aurez plus de mal à vous endormir (on explique pourquoi en fin d’article).

Idem pour le matin, définissez une règle qui dit approximativement que: "_Je n’allume rien de communiquant tant que je n’ai pas mangé et que je ne suis pas douché_".

#### Relativiser l’importance de vos communications

Pensez que si il n’y a pas danger de mort annoncé la veille, vos communications ne sont pas vitales et moins prioritaires que votre bien-être ou votre occupation du moment. Un mail peut attendre, un SMS aussi, idem pour les réseaux sociaux.

#### Quand on mange, on mange !

Le repas, selon votre âge, est très souvent accompagné d’un téléphone, d’autant plus pour ceux qui n’ont pas le droit de l’utiliser durant leur temps de travail. Le midi, essayez autant que possible de ne pas utiliser votre téléphone et préférez les communications humaines "en vrai".

Notez aussi que bidouiller votre téléphone en mangeant, c’est pas top respect pour ceux qui vous entourent (on nomme ça le _phubbing_)…

Le soir, selon votre situation, que vous mangiez avec votre conjoint(e) ou vos enfants, gardez-vous ce temps pour échanger avec ces personnes. Si le téléphone sonne (fixe, car vous aurez pris soin d’inclure le repas du soir dans la plage horaire de non utilisation), n’y répondez pas. Vous pourrez toujours contacter la personne après le repas (avec le fixe, n’en faites pas une excuse pour rallumer votre smartphone).

Aussi, (et c’est surtout pour les jeunes), ne mangez pas devant votre ordinateur. Hormis quelques soirées pizzas / bières / console bien sûr !

#### Rallongez vos temps de réponse

Un peu redondant avec les précédentes règles : prenez le temps de ne pas répondre. Faites une activité après une autre et n’interrompez pas votre occupation en cours pour répondre à un vulgaire "_kikoo, sa va ? lol_" sur <del>Facebook</del> TroncheLivre.

#### Désinstallez les applications de réseaux sociaux

Si vous êtes pas trop atteint et que vous ne voulez pas le devenir plus, désinstallez les applications de réseaux sociaux de votre smartphone. Certes c’est radical. Si par contre, vous êtes un peu plus nomophobe que la moyenne, ne le faite pas dès le début, c’est un peu trop "brutal" comme méthode. Il faut y aller progressivement pour éviter trop de frustrations, qui participeront à la diminution de votre motivation.

### Point commun

Comme indiqué avant, on retrouve un point commun avec toutes les luttes contre des différentes addictions (alcool, cigarette, etc…): la motivation.

Sans elle, point de victoire. Il vaut mieux la baser sur les sentiments et l’affect que sur les choses matérielles ou l’argent. Aussi, la motivation la plus puissante est celle qui vient de vous. Si vous êtes amené à travailler sur une addiction, il faut que ça vienne de vous, ne le faites pas pour les autres ou ça ne durera pas et le risque de rechute sera grand.

Pour ceux souhaitant en savoir plus sur la motivation et sa théorisation psychologique, je vous invite à vous procurer l’excellent ["Traité de psychologie de la motivation"](http://www.dunod.com/sciences-sociales-humaines/psychologie/psychologie-cognitive/master-et-doctorat/traite-de-psychologie-de-la-motivation) de Philippe Carré et Fabien Fenouillet au éditions DUNOD pour environ moins de 40€.

### L’utilisation d’un écran avant de dormir diminue la sensation de sommeil

Oui, les écrans et de manière générale s’exposer à une source trop lumineuse durant un certain temps avant d’aller se coucher perturbe votre sommeil.

Pourquoi ?

La glande pinéale, ça vous parle ? En gros, c’est une glande endocrine (donc qui sécrète des hormones) située dans le cerveau. Elle secrète de la mélatonine à partir de la sérotonine. Ladite mélatonine joue un rôle important dans la régulation de votre rythme biologique et notamment, les cycles du sommeil.

Pour faire simple, c’est la mélatonine qui donne le signal au corps pour qu’il entre dans une phase propice au sommeil. Des études émanant de l’Institut Polytechnique Rensselaer (New York) montre qu’après deux heures devant un écran, les taux de mélatonine baissent (entre 3 et 6 fois moins!) et donc le sommeil se fait moins ressentir. Notez que plus vos yeux reçoivent de la lumière bleue (issue de la lumière blanche du soleil ou des écrans par exemple) et plus la production de mélatonine est faible, ce qui fait que en journée, nous avons moins cette sensation de sommeil, tout comme à 3h du matin devant un écran.

La production de mélatonine quand on est face à un écran joue avec trois facteurs:

* La proximité écran / œil
* Le taux de luminosité
* La proportion de bleu que contient la lumière issue de votre écran

Pour adapter la luminosité de votre écran, l’ami [@maxauvy](http://open-freax.fr/) a écrit un article concernant [Redshift](http://open-freax.fr/redshift-reposez-vos-yeux/), un logiciel libre qui adapte la température des couleurs de votre écran selon votre emplacement géographique (et donc l’heure). L’affichage de votre écran sera plus orienté vers l’orange ou le rouge, des couleurs qui naturellement agressent moins vos yeux en soirée (votre rétine baigne dans le sang, origine de l’effet « yeux rouges » sur des photos à simple flash). C’est une bonne technique pour reposer un peu plus vos yeux si vous êtes amenés à travailler souvent sur un écran, de nuit (par obligation ou par choix) ainsi que de préserver les signaux de votre corps, comme ceux liés au sommeil.

### L'hiver me fait déprimer...

Une dernière chose concernant la mélatonine. Dans des pays aux hivers longs et gris (typiquement en Europe du Nord et au Canada ou parfois en France), la baisse générale de luminosité à cause des jours raccourcis et du mauvais temps provoque une surproduction de mélatonine et peut engendrer une dépression (un TAS : Trouble Affectif Saisonnier) allant de simples troubles de l’humeur jusqu’à un réel syndrome dépressif. Notez que la femme est plus exposée à ces TAS que l’homme, même si ça tend à s’uniformiser avec l’âge. J’insiste sur ce point, car je sais que certaines de mes lectrices à l’approche de l’hiver ressentent déjà cette sensation de dépression… ;)

Pour contrer ses dépressions, il existe des séances de luminothérapie ayant pour but de vous exposer à une forte lumière à grande concentration de bleu pour inhiber les récepteurs de la glande pinéale et donc diminuer la production de mélatonine et par conséquent, d’abaisser le niveau de dépression.

### Fin

Ce type d’article s’éloigne un peu de ce que je traite habituellement (plus orienté informatique), mais ça allait aussi dans la suite logique des articles sur l’expérience smartphone et ça reste des sujets intéressants. Il me semble que diversifier le contenu ne fait pas de mal et peut aider le lecteur à lui faire découvrir des choses, hors de son domaine de compétence. Encore une fois, si vous souhaitez réagir, n’hésitez pas !