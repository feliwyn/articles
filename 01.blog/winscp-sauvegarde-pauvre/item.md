---
title: WinSCP, ou la sauvegarde du pauvre
date: 17-10-2012 12:00
taxonomy:
	category: blog
	tag: [sysadmin]
---

Un tutoriel rapide pour mettre en place une solution de sauvegarde très simple mais puissante, sous Windows.

===

Amis Windowsiens, cet article est pour vous (c'est le seul de tout le blog).

Aujourd’hui un collègue me dit: 

>Bah, j’fais mes backups avec WinSCP, ça tue !

Paroles de Windowsien que je n’aime pas, mais soyons ouverts, je vais tester. Difficile pour moi vu qu’à ma connaissance, WinSCP n’est pas disponible sur FreeBSD et que de toute façon, j’utilise un autre système de sauvegarde (enfin plusieurs).

Mais je me suis pris au jeu et j’ai monté une machine virtuelle pour tenter de faire des backups avec WinSCP depuis windows sur un serveur virtuel Debian. J’ai nommé ceci la sauvegarde du pauvre (histoire de troller un peu après tout), même si c’est fonctionnel et pas trop embêtant. Bon, je l’avoue, c’est franchement puissant…

### Objectifs

* Ne pas balancer moult euros dans un logiciel de sauvegarde.
* Faire ses sauvegardes tout seul comme un grand sans dépendre d’un logiciel gratuit qui comble la moitié de vos besoins. Quoi de mieux que de faire ses sauvegardes perso sans prise de tête ?
* Ne pas se prendre la tête avec des trucs chiants pour un Windowsien (cygwin + rsync).
* Une fois en place, lancez toutes vos sauvegardes d’un double clic.

C'est parti !

### Backuper un serveur de manière sécurisée via SFTP / SCP


Non, ça ne fonctionne pas avec des bouts de ficelle et des pots de yaourt.

Au total, il nous faut:

* Une version PORTABLE de WinSCP, [ici même](http://winscp.net/eng/download.php)
* Un bloc note (un éditeur quoi)
* De l’espace disque
* Un serveur accessible en SFTP ou mieux, en SCP

### Un peu d'organisation

Créons les dossiers suivants:


* Créez un dossier sur votre disque nommé winscp
* Créez 3 sous dossiers nommés bin (contiendra l’exécutable de winscp), inc (contiendra nos scripts) et log (contiendra… les logs !)

Les noms peuvent être différents, mais ceux-ci sont explicites.

Notre dossier contenant le backup sera `D:BACKUP` (oui, pas très original, mais ça parle à tout le monde). Avec des sous-dossiers pour sauvegarder un peu ce qu’on veut, comme `D:BACKUPMAISON` pour /home/moi et `D:BACKUPSITE` pour /var/www.

Dézippez votre version **portable** de WinSCP, copiez `WinSCP.exe` dans `bin`, renommez en `winscp.exe` (en minuscule, parce que j’aime pas les scripts avec des majuscules).

Pour l'instant, votre dossier `winscp` contient:

* `bin` contenant `winscp.exe`
* `inc` ne contenant rien
* `log` ne contenant rien

Lancez winscp.exe puis configurez une nouvelle connexion. Ci après, un petit tableau pour vous aider à remplir les champs:

| Champs | Utilité |
|--------|---------|
|Hostname| le nom ou l'ip du serveur|
|Username| un utilisateur autorisé à se connecter en SSH|
|Password| mot de passe de l’utilisateur|
|Port| port ssh du serveur|
|Protocole| SFTP ou SCP selon vous|


Cliquez sur `Save` et donnez un nom à votre connexion (pour cet exemple, ça sera `backup`). Fermez WinSCP.

### Scriptons !

On va s’attaquer au script WinSCP pour rapatrier plusieurs répertoire sur notre `D:BACKUP`, placez-vous dans le répertoire `winscpinc` et créez un fichier texte nommé `backup.script` (affichez les extension pour éviter de créer un backup.script.txt).

Nous allons écrire le contenu suivants dans le script:

```bash
option batch on
option confirm off

open backup
option transfer binary

synchronize local D:BACKUPMAISON /home/mon_utilisateur -delete -criteria=size -nopermissions

synchronize local D:BACKUPMON_SITE /var/www -delete -criteria=size -nopermissions

close
exit
```

`open backup` sert à lancer la connexion que nous avons paramétré plus haut (nommée `backup`). Le reste des commandes est assez simple. On synchronise le dossier local avec le dossier distant en supprimant les dossiers obsolètes (`-delete`) déterminés par une comparaison de taille (`-criteria=size`) tout en gardant les permissions des fichiers (`-nopermissions`).

Passons maintenant au script bat qui va lancer ceci.

Placez-vous dans le dossier `winscp` et créez un fichier nommé `backup.bat` (idem, attention aux extensions).

On y écrit le contenu suivant:

```sh
@echo off

del loglogbackup.log

binwinscp.exe /console /script=incbackup.script /log=loglogbackup.log
```

Simple, on efface le fichier de log présent dans `winscplog`, on lance l’exe de winscp présent dans `binwinscp` dans une console en lui indiquant de lancer le script `backup.script` présent dans le dossier `winscpinc` et on journalise le tout dans un fichier nommé `backup.log` dans le fichier `winscplog`.

Double cliquez sur `backup.bat` pour lancer le backup et c’est fini ! Si toutefois vous avez coupé avant la fin, au prochain lancement il reprendra là où il s’est arrêté, pratique.

### Conclusion

Notre disque dédié à la sauvegarde est D:. Il contient:

* `D:BACKUP` qui accueillera nos données sauvegardées avec en sous répertoire:
    * `MAISON` qui accueillera les sauvegarde de notre `/home/moi`
    * `MON_SITE` qui accueillera les sauvegarde du site présent dans `/var/www`
* `D:winscp` contenant:
    * `bin`, dossier contenant `winscp.exe` (et `winscp.ini` si vous l’avez déjà lancé)
    * `inc`, dossier contenant nos scripts de sauvegarde
    * `log`, dossier contenant les logs générés par les sauvegardes
    * `backup.bat`, l’exécutable qui permet de lancer le script de sauvegarde avec winscp

C’est donc franchement simple et très performant. Pour restaurer vos sauvegardes, il suffit de changer `local` par `remote` dans vos scripts pour faire l’effet inverse. Donc écrire le contenu de `D:BACKUPMAISON` dans `/home/moi` et `D:BACKUPSITE` dans `/var/www`, tout en gardant les permissions des fichiers. De quoi remettre facilement et rapidement en place les dossiers et documents d’un FTP qui aurait eu des soucis par exemple.

C’est un exemple assez basique mais déjà puissant qui permet une synchro distante. Bien sûr la première fois que vous touchez à WinSCP via des scripts, faites le sur un système à part et non en production ! En effet, ça peut être dangereux surtout si vous mélangez qui est local et qui est distant, et qui écrase qui.

Le script de synchronisation est assez long la première fois car il récupère tout le contenu, par contre au lancement suivant, il n’écrira que la différence et donc, sera plus rapide.

Et comme on est pas des gros bourrins et qu’on est mignons, on fait 3 copies sur 3 supports différents entreposés dans 3 lieux géographiques différents à plus de 5km les uns des autres.

Pour ceux qui veulent aller plus loin, [allez donc faire un tour par ici](http://winscp.net/eng/docs/scripting#commands) !