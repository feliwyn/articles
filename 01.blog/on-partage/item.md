---
title: On partage ?
date: 17-03-2016
taxonomy:
    category: blog
    tag: [unixmail, git]
---

Après le petit mot de Géraldine, je me suis rendu compte d'une chose: Ce n'est vraiment pas pratique de venir poster ici, ou même de signaler un simple bug. Cet article vous raconte tout ce qui a été fait pour vous simplifier la vie si vous souhaitez participer, signaler une simple correction ou même écrire un article complet.

===

### Tweet ou pigeon voyageur

C'est souvent que l'on me signale des coquilles dans les articles. J'ai des gros doigts, je me relis trop rapidement et donc j'en laisse partout. Vous me l'indiquez par Twitter, un coup c'est par mail, des fois de vive voix, bref, les moyens sont divers et variés et quand j'en ai trop à corriger (parce que j'ai vraiment des gros doigts) je perds le fil de ce que j'ai fait, ou pas fait.

Agaçant non ?

Donc depuis peu, j'ai décidé de _versionner_ l'intégralité du contenu éditable du blog. A savoir toutes les pages et tous les articles au format Markdown, c'est le format utilisé ici. Comme ça, c'est simple, j'ai un historique de toutes les modifications sur tous les articles et je sais ce que j'ai fait ou pas. C'est bien mais si j'ai trop de choses à traiter, la moitié passe à la trappe, et Git ne me permet pas d'avoir une trace de ce qu'il reste à faire (un genre de _roadmap_ mais en plus simple)...

C'est là que je me suis dit: _"Mais ! Si tous tes articles sont versionnés, laisse donc la possibilité aux visiteurs de signaler et même de corriger le contenu pour les plus motivés !"_

### Participez !

J'ai donc ouvert le dépôt [Gitlab](https://gitlab.com/Zilkos/articles) qui assure la gestion des versions du contenu éditable de ce blog. Plusieurs choses sont possibles:

#### Signalez, complétez, proposez, écrivez...

La chose la plus courante, c'est le signalement. Le dépôt Gitlab dispose de ce qu'il faut pour ouvrir des _issues_ ("problème" en français). Une _issue_ est tout simplement une entrée de suivi dans le dépôt qui permet d'indiquer un problème, une erreur, un signalement, une proposition, etc.

Afin de ne pas perdre le fil et surtout de ne pas tout mélanger, j'ai créé des labels qui permettent de classer les catégories des _issues_. Ces labels sont les suivants:

  + Lien
    - Pour signaler un lien mort ou en erreur
  + Typo
  	- Pour signaler des fautes, de mauvaises tournures de phrase, une mauvaise mise en page
  + Qualité des propos
  	- Je ne suis pas expert en tout et il se peut que je me trompe, cette catégorie est dédiée aux erreurs de fond et non pas de forme.
  + Proposition d'article
  	- Si vous avez une idée d'article, ou un article déjà rédigé, proposez le et on en discutera !

Il y en aura peut-être d'autres, au fil du temps.

Si vous créez une _issue_ et que vous n'êtes pas habitué à utiliser Gitlab ou Github, remplissez simplement le squelette qui est déjà pré-rempli dans l'issue, indiquez le label et validez. Pour les habitués, vous savez déjà comment faire et vous pouvez vous passer du squelette.

Pour proposer du contenu, il suffit de _forker_ le dépôt, d'ajouter votre contenu et de faire une demande de _merge-request_. Bien évidemment, on discutera de tout ça, pour vérifier si c'est dans la lignée du blog. Dans ce cas là aussi, on en discutera avec les éventuels visiteurs qui passent par là, tout le monde peut intervenir sur une issue pour donner son point de vue ou proposer d'autres choses. Certes, il n'y a pas spécialement de ligne éditoriale, mais je ne pense pas qu'un article sur la couture soit pertinent ici (même si la couture, c'est chouette).

Pour ceux qui souhaitent juste proposer une idée, ouvrez une _issue_ et on en discutera ! Bien évidement, tout le monde peut intervenir sur les _issues_ des autres et par conséquent, sur tous les articles. Vous pouvez aussi commenter chaque _commit_, et même chaque ligne de chaque fichier si le coeur vous en dit !

#### Lumière sur vous

Je ne vole pas les écrits des gens. Si vous souhaitez soumettre un article, il sera en **votre** nom et soumis à la licence CC active sur ce blog. Si vous participez à un article, vous serez dans les remerciements de l'article en question, idem pour un signalement ou toute autre participation. Je vais tenir dans la _sidebar_ de ce blog une liste des contributeurs (quand il y en aura). Bien évidement, si vous voulez faire ça sans être nommé, il suffit de l'indiquer.

D'ici quelques jours, je finirai sur le Gitlab une partie nommée _Guide de contribution_ qui vous indiquera comment construire un article avec Markdown pour qu'il soit en adéquation avec ce blog.

J'en profite pour vous remercier de lire les quelques bafouilles que j'arrive à pondre une fois de temps en temps et vous remercier pour vos messages, mails et tweets de satisfaction. J'ai noté que certaines personnes étaient contentes de revoir du contenu ici, sachez que c'est un plaisir que de faire revivre ce bout de la toile et c'est un plaisir de commencer une éventuelle collaboration avec vous, si minime soit-elle.

Et si on se croise un jour, rappelez moi de vous offrir une bière (ou deux).

  - [Le dépôt Gitlab](https://gitlab.com/Zilkos/articles)
  - [Les bases de Git](https://git-scm.com/book/fr/v1/Les-bases-de-Git) pour les plus barbus.
  - [La syntaxe Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)