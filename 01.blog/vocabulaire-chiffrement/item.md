---
title: Un point de vocabulaire de chiffrement
date: 14-10-2012 12:00
taxonomy:
	category: blog
	tag: [crypto]
---

Afin de se faire comprendre par tout le monde, posons des mots et des explications sur le vocabulaire du chiffrement.

===

J’ai passé ma soirée d’hier avec un barbu bidouilleur exilé au pays des lagopèdes qui m’a grandement conseillé sur le chiffrement de mails via des clef PGP/GPG. Nous discutions sur Jabber et à chaque fois, je m’emmêlais les pinceaux avec chiffrement, cryptage, décryptage et déchiffrement.

J’ai donc décidé de faire lumière sur ces quelques mots, d’un point de vue cryptographique et verbale.

#### Crypto quoi ?

* **Cryptologie**: Veut tout simplement dire la science (_logie_) du secret (_crypto_). C’est l’art et la manière de faire passer des messages chiffrés à l’aide de clefs. 

Le plus grand exemple est sans doute celui de Jules César qui utilisait un chiffrement par décalage, rendant un message écrit incompréhensible si il tombait dans les mains de l’ennemi.

C’est tout simple. Si on utilise un décalage de 3 lettres (ce décalage représente _la clef_) le message devient incompréhensible, sauf si vous avez la clef. Ainsi `BONJOUR` avec un décalage de 3 donne  `ERQMRXU`. Plutôt malin mais avec quelques neurones (ou de la potion magique), on en vient vite à bout.

* **Cryptographie**: La cryptographie, qui fait partie de la cryptologie, signifie "l’écrit (_graphie_) du secret (_crypto_). C’est la protection de la compréhension d’un message par clef.

* **Chiffrement**: C’est transformer un message clair en un message incompréhensible (chiffré) à l’aide d’une **clef de chiffrement**. Si vous n’avez pas la clef, vous ne pourrez pas procéder au déchiffrement.

* **Déchiffrement**: C’est logiquement l’opération inverse du chiffrement, **en ayant la clef**.

* **Décryptage**: C’est le fait d’essayer de trouver le message clair d’un message chiffré **sans en posséder la clef**. C’est donc en quelque sorte du déchiffrement, mais sans clef, donc du décryptage.

Donc si vous avez correctement suivi, vous devez me dire:

>>>>>>Si décrypter, c’est essayer de trouver le message clair sans la clef, le mot crypter n’a aucun sens ! 

Parfait !

Donc au final, on **chiffre** un message avec un clef, on **déchiffre** un message avec une clef et on **décrypte** un message si on a pas la clef ! A aucun moment on crypte un message, sauf certains québécois qui l’utilisent au sens _chiffrer_, comme précisé dans le [GDT](http://www.gdt.oqlf.gouv.qc.ca/).

C'est tout pour ces quelques précisions, tendez l'oreille, et vous entendrez plein de choses moches sur le chiffrement !