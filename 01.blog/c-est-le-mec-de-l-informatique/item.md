---
title: C'est le mec de l'informatique.
date: 16-03-2016
taxonomy:
    category: blog
    tag: [histoire, géraldine]
---

Salut visiteur, c'est Géraldine. Comme Zilkos est super sympa, il m'a prêté un bout de son ordinateur de l'Internet pour que je vous parle d'un mec bizarre qu'il y a au boulot. Le mec de l'informatique.
Rassurez vous, je fais court !

===

>>>>>> Visiteur, je tiens à t'avertir que ceci est une fiction. Une simple histoire et rien d'autre. Tout ceci est aussi fictif que la Géraldine en question. <i class="fa fa-heart"></i> sur toi. 

Il passe tous les matins devant mon bureau, en marmonnant un léger _"Bonjour Géraldine"_ dans sa barbe visiblement non taillée. Habillé comme un "jeune", il a tous les jours un t-shirt différent avec des trucs écrits dessus. Des trucs sans le moindre sens comme _"There is no place like 127.0.0.1"_ ou autre _"In God we trust. s/God/code/"_. Encore un délire de jeune.

Il traîne la savate jusqu'à son bureau, probablement qu'il a joué à des jeux débiles jusqu'à 4h du matin. Avec une vie pareille, des fringues comme ça et sa manière bizarre de s'exprimer, je me demande bien comment il a pu trouver une copine. Enfin bon, à cet âge-là, elles ne sont pas regardantes. Je l'entends arriver dans son bureau, il pose son sac, allume son ordinateur portable, son deuxième écran, désactive le renvoi d'appel sur son téléphone fixe, comme s'il recevait des appels à 21h le soir sur son mobile franchement... N'importe quoi. 

Il s'assoit un instant pour taper son mot de passe à rallonge, encore une lubie de mec dans son genre, _toto_ c'est quand même plus court, surtout si on doit le taper 50 fois par jour. Enfin bon, c'est comme ça. Fut un temps, on avait des mots de passe compliqués, donc forcément on l'écrivait sur un post-it, bah même avec un mot de passe compliqué, il n'était pas content parce qu'on l'écrivait... N'importe quoi. Bref.

Il se lève et fait le tour de tous les bureaux pour dire bonjour. Ça, on ne peut pas lui enlever, il est très sympathique quand il veut. Bureau après bureau, il alterne entre bonjour timide et franche bise, échange quelques mots avec certains collègues, puis retourne à son bureau pour taper des trucs abscons sur son ordinateur. Il lance des fenêtres noires avec des écritures vertes, c'est moche comme c'est pas permis. C'est sans doute un style qu'il se donne, des couleurs partout, des écritures plus incompréhensibles les unes que les autres. On a vraiment l'impression avec les collègues que le jeune homme est resté bloqué dans l'informatique des années 80, des trucs moches, pas d'image et visiblement sa souris lui sert peu. On est tous persuadé qu'il se donne un genre avec ces trucs-là. 

Il baille à s'en décrocher la mâchoire, sans doute que le café n'a pas encore fait effet. En même temps, les jeux vidéos, c'est bien connu, ça rend violent et cela empêche de dormir. C'est triste quand même hein ? Nos jeunes qui s'abrutissent sur des jeux au lieu d'apprendre des choses avec les émissions à la télé. On aime bien ici, papoter au café sur les télé-réalités qu'on a vu la veille ou d'autres émissions. On parle beaucoup de Tellement vrai, ou d'autres émissions de société du genre, c'est vraiment des gens dans une détresse et on y apprend plein de choses sur l'éducation de nos enfants. Ils regardent ce genre d'émissions nos enfants d'ailleurs, ils trouvent ça rigolo.

Bref, je ne vais pas vous expliquer que la télévision permet quand même d'être plus au courant de l'actualité que de s'abrutir devant des jeux vidéos hein ? Tout le monde le sait, sauf lui visiblement.

Il est quand même très barbu pour un "français". L'autre jour on lui a dit pour rigoler que c'était un terroriste qui se radicalisait. Rien de bien méchant hein, seulement une blague pour rigoler sur sa barbe. Bon visiblement il n'était pas de bon poil, il a répondu des trucs qu'on n'a pas trop compris, ça parlait d'amalgame avec je sais pas quoi et il a parlé de nos enfants aussi, bref, encore des trucs bizarres de sa part, on est habitués. 

Il râle souvent aussi, pas forcément pour grand chose, mais il râle au moins une fois par jour. Par exemple l'autre jour, j'avais plus mes fichiers dans Excel. Je vais le voir et je lui demande _"Pourquoi mes tableaux Excel ont disparu" ?_ Il m'a demandé où je les avais rangés, bah _"Dans Excel !!"_ que je lui réponds, normal ! Au final, il daigne lever son fondement de sa chaise et vient sur mon poste. Je lui montre que quand je fais "Fichier > Ouvrir" la liste des fichiers est vide, il n'y a plus rien dans mon Excel.

Et là il commence à m'expliquer qu'en fait, ce sont des "emplacements récents" et que mes tableaux sont enregistrés ailleurs... Je pense qu'il n'a pas dû comprendre ce que je voulais, je lui dis que j'ai plus de tableaux Excel et il me dit qu'ils sont ailleurs... J'ai TOUJOURS enregistré et ouvert mes fichiers avec Excel alors qu'il ne me prenne pas pour un jambon hein !

Bref, maintenant c'est le foutoir sur mon poste de travail, il y a passé 15 minutes pour "ranger" mes tableaux Excel qui étaient dans Excel et maintenant ils sont dans des dossiers.

Pourtant, le mec est ingénieur à ce qu'il parait, il devrait connaitre un peu plus que ça...

L'autre jour, Martine n'était pas là de la journée, je lui demande si je peux accéder son poste et s'il a le mot de passe. Il me répond _"Non seulement il te faut le mot de passe que je ne connais pas, mais aussi son autorisation pour utiliser son poste, c'est indiqué dans la charte informatique"_. Juste hallucinant ! De toute façon, on a un système informatique vieux et qui ne fonctionne pas. Sachez qu'ici, on ne peut même pas envoyer un tableau Excel par mail, VRAIMENT NUL ! Il m'a dit _"C'est beaucoup trop volumineux 150Mo par mail, faut passer par un partage"_ ou je ne sais pas quoi, en même temps "Mo", "Go", qu'est-ce qu'on s'en fout.

Le pire c'est que des fois, il nous fait attendre ! On va le voir pour un truc et il nous répond _"Je passe dans une quinzaine de minutes, je termine un truc"_ et il se remet à taper frénétiquement dans sa fenêtre sans couleur et sans image... Je suis sûr qu'il le fait exprès juste pour nous embêter, de toute façon, je me demande bien ce qu'il peut faire de ses journées.

Je crois que le pire c'est par mail... Nous on envoie des mails avec la vraie signature de la boite, le logo dans la signature et tout, un truc propre, professionnel. Et bien lui, non. Il envoie des mails où il ne peut même pas mettre en couleur, ni mettre de tableau ni rien, il a une police d'écriture moche digne des années 70 alors que nous, on peut souligner, mettre en gras, enfin comme dans Word. Comme quoi, c'est bien les cordonniers les plus mal chaussés ! Bon par contre les mails c'est bien, mais on a un quota, et franchement, il doit pas être bien gros... Il faut faire du ménage en permanence sinon c'est vite plein. Lui il n'a jamais de problème comme ça, de toute façon il se réserve tout pour lui je suis sûre. Un genre d'avantage du métier.

C'est quand même dommage, parce que je suis sûr qu'il pourrait être gentil s'il levait le nez de ses machines de temps à autres. En changeant deux ou trois choses, il pourrait être bien ce jeune homme. Déjà, au niveau vestimentaire, un pantalon un peu habillé et une chemise ça ne coute pas grand chose et il serait déjà plus compétent et crédible. Là, avec son t-shirt, on a vraiment du mal à croire qu'il sait faire des choses de truc d'ingénieur en ordinateur. Pas très crédible tout ça. Se raser, c'est pas compliqué ! Pourquoi il y tient tant, à sa barbe moche ? Ça fait vraiment négligé, et par les temps qui courrent, on évite de porter ce genre de chose si on ne veut pas se faire mal voir ou passer pour un djihadiste, à croire qu'il cherche les ennuis.

Mais bon, que voulez-vous, ça reste un jeune du monde digital des ordinateurs... 

Quel fléau.