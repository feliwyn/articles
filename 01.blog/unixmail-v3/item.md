---
title: Unixmail V3 est dans la place !
date: 06-03-2015 12:00
taxonomy:
	category: blog
	tag: [blog]
---

Un tout nouveau moteur de blog, une nouvelle présentation et plein d'autres choses pour cette troisième version d'Unixmail ! Je vous résume les principales nouveautés dans cet article, ainsi que les raisons des changements !

===

#### Pourquoi une V3 si rapidement ?

Wordpress. Juste Wordpress, ce n'est plus possible.

Lourd, lent, pour le peu de contenu que j'ai sur ce blog, déployer une telle armée de saloperies plus lourdes les unes que les autres, c'est scandaleux. Sans compter plein de choses qui m’agaçaient, notamment le fait qu'il se mette à jour tout seul, sans rien me demander...

Des mises à jours de plugins tous les jours ou presque, obligé de déployer une armée d'anti-spam pour les commentaires et j'en passe. Bref, un système basé sur une base de données principalement inondée de choses inutiles, les article ne représentant quasiment rien dans cette base.

Pour vous donner un ordre d'idée, la dernière sauvegarde d'Unixmail V2 fait 140Mo (taille sur disque) sans la base de donnée.

La V3 fait 10,8Mo (taille sur disque) soit **12 fois moins** pour le même contenu, ça se passe de commentaire.


#### What's up Doc' ?

Un tout nouveau moteur de blog **statique** ! Oui, j'ai opté pour un _file-based CMS_. Ce blog ne fonctionne plus avec une base de données lourde et lente, mais avec uniquement des fichiers en texte brut, et du PHP. Beaucoup plus rapide, plus souple, et l'écriture des articles se fait hors-ligne, en Markdown !


##### Perte de mise en forme de contenu ?

Non, du tout, au contraire, j'ai d'autres méthodes pour mettre en valeur le contenu car ce n'est pas du simple Markdown qui est utilisé, mais du [Git Flavored Markdown](https://help.github.com/articles/github-flavored-markdown/) qui me donne beaucoup de souplesse dans l'écriture, couplé à du Bootstrap et du Font Awesome.

Avec tout ceci, j'ai conservé les conventions de mise en forme de contenu établies sur la V2, par exemple les blocs de couleur (rouge / orange / vert/ bleu):

>>>>>> Les trucs cools

>>>> Les trucs pas cools

Ainsi que des tableaux, et plein d'autres mises en forme apportant du sens au contenu.


##### Un confort de lecture

Certes, c'est assez subjectif, mais je trouve cette version apporte un meilleur confort de lisibilité. La police est plus adaptée à la lecture sur écran et si toutefois il y a un bloc de code, il s'intègre correctement dans "le paysage":

Code Perl:

```perl
#!/usr/bin/perl -w
use strict;
use Net::FTP;
my $ftp = Net::FTP->new("ftp.cpan.org",
   Debug => 0, Passive =>1 ) or die("$!");
$ftp->login("anonymous",'-anonymous@');
$ftp->cwd("/pub/CPAN/");
$ftp->get("ls-lR.gz");
$ftp->quit();
```

Code Java:

```java
package com.test.zilk;

import org.zilk.Document;
import org.zilk.output.Format;
import org.zilk.output.XMLOutputter;

public abstract class TestJDOM {

  protected static void afficher(Document document)
  {
     try
     {
        XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
        sortie.output(document, System.out);
     }
     catch (java.io.IOException e){}
  }

}
```

C'est vraiment un bonheur que d'écrire des articles en Markdown. D'autant que les codes sources ne sautent pas, ni l'indentation ! Les articles sont sur un Git maison pour gérer les versions en cours de rédaction, ainsi je peux rédiger n'importe où, même hors ligne, sans être forcé de passer 3 ans à mettre l'article en forme. Encore une fois, le texte brut, c'est la vie !


##### Refonte des articles

J'ai codé une moulinette maison qui récupère le code HTML des anciens articles pour les transformer grossièrement en Markdown, histoire d'alléger la masse de travail. Un truc maison, un peu bancal mais qui fait le plus gros. Au final, j'ai relu tous les articles et j'ai corrigé plein de choses, j'en ai réécrit la plupart, mis à jour certains et complété d'autre, notamment les articles les plus lus. Par exemple, [le guide sur Emacs](http://www.unixmail.fr/blog/emacs-guide-debutant) a été réécrit de A à Z.

J'ai supprimé plein de vieux articles n'ayant plus lieu d'être. Rassurez-vous, j'ai sauvegardé la totalité des articles, certains "vieux" reviendront peut-être un jour si nécessaire.

Plus de smiley. Ce n'est pas très sérieux, et ça n'apporte rien au contenu. Au pire, ils sont en version texte, mais pas d'image.


##### Adieu les commentaires

Fini les commentaires. La plupart n'étaient que peu constructifs et il n'y en avait pas assez pour les garder sur cette version. A y réfléchir, il n'y a jamais eu de vrais débats dans les commentaires. Si vous avez des suggestions, rapport de bogue ou autre, c'est par mail (chiffré, bien entendu).

Pour les commentaires qui apportaient du sens aux article, l'idée a été intégrée à l'article.

##### Bonjour l'édition collaborative

Si vous voulez écrire un article avec moi, aucun soucis, un simple fichier Markdown sur un Git ou sur un Owncloud ou même un Framapad fait l'affaire !

N'hésitez pas à m'envoyer par mail vos idées des articles ou si vous souhaitez me voir aborder un sujet en particulier. Je manque cruellement d'idées ces temps-ci !

##### Organisation du contenu

Plutôt que d'opter pour des catégories, j'ai préféré les tags. Ce système est plus simple. Sur la droite, dans la _sidebar_ vous trouverez la liste des tags. En cliquant sur un tag, vous accéder à une page présentant tous les articles pour le tag sélectionné. Plus simple, plus clair, plus intuitif.

Lors de l'écriture d'un article j'ai un _template_ (une cartouche en haut du fichier markdown) qui est analysé par le système pour classer automatiquement l'article en prenant en compte la date et les tags, donc je ne range rien à la main, tout se fait tout seul.

Lors de l'affichage d'un article en pleine page, vous trouverez dans la _sidebar_ des suggestions de lecture en relation avec l'article courant, c'est toujours bien pour les nouveaux venus. Attention, c'est basé sur les tags et l'analyse du contenu. Si c'est un nouveau sujet, il se peut que cette liste ne présente pas d'entrée.

##### En avant !

J'espère que cette nouvelle version vous plaît, elle a été construite en prenant en compte la plupart des défauts de l'ancienne version et surtout, elle a été faite en pensant prioritairement au confort du visiteur. Rapidité, simplicité, et confort de lecture.

Comme lire sur un téléphone n'est pas confortable, je ne me suis pas cassé la tête avec le _responsive-design_ pour les petits écrans, ça passe, mais globalement, le site n'est pas très adapté aux téléphones mobiles.

Peut-être qu'il y aura des ajustements au fur et à mesure du temps et selon les bugs et autres points de non confort relevés par les visiteurs.

A bientôt sur Unixmail ! 