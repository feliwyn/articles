# Unixmail.fr - Articles

Ce dépôt contient tous les articles ainsi que toutes les pages du contenu éditable du blog. Il sert aussi d'outil pour soumettre un bug, un souci dans un article, ou même si vous souhaitez publier votre article rien qu'à vous !

## Contributions
### Modifier ou ajouter du contenu vous-même

Si vous êtes dans un ou plusieurs des cas suivants:

  - J'ai vu une coquille dans un article que je souhaite corriger (faute, lien mort, mise en page douteuse, propos non précis)
  - Je souhaite modifier en profondeur un article (ajouter une partie, ajouter des précisions, etc.)
  - J'ai un article complet à proposer
  
Dans ces cas-là, n'hésitez pas à _forker_ le dépôt et à ouvrir une _merge request_ une fois vos modifications effectuées. Bien évidemment, on discutera de votre article par mail ou IRC afin de s'assurer du fait que l'article ait un minimum de rapport avec le contenu du blog (non pas que je n'aime pas la couture ou la pêche aux moules, mais bon).

Bien évidemment, vos modifications sont en votre nom, je ne m'attribue pas votre travail.
Si vous postez un article complet, il sera en votre nom, avec vos propos, vos explications, et votre nom dans la partie "Contributeurs" de la sidebar (qui pour l'instant n'existe pas).
Si vous effectuez une modification sur un article, votre nom sera en bas de page dans les remerciements et sur la sidebar en tant que contributeur.

### Signaler une erreur ou papoter à propos d'un article / d'un propos
Si vous êtes dans les cas suivants:

  - J'ai vu une coquille, je souhaite la signaler.
  - Je voudrais soumettre une idée d'article / un sujet / un thème
  - J'ai un doute quant à la qualité d'un propos, je suspecte une erreur et souhaite en discuter
  
Je vous invite à ouvrir une _issue_ pour qu'on en parle ensemble et/ou avec les autres visiteurs du blog.

N'hésitez pas, je ne suis pas méchant, j'ai plus tendance à offrir des bières aux contributeurs qu'à les mordre.

EN COURS: Rédaction du guide de contribution.