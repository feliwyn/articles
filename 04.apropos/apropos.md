---
title: A Propos
blog_url: apropos
body_classes: header-image fullwidth

sitemap:
    changefreq: monthly
    priority: 1.03

content:
    items: @self.children
    order:
        by: date
        dir: desc
    limit: 5
    pagination: true

feed:
    description: Fourre-tout Libre
    limit: 10

pagination: false
---

# <i class="fa fa-gavel"></i> A propos
## Et mentions légales
