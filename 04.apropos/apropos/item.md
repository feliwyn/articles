---
title: 'A propos d''Unixmail...'
date: '27-12-2013 00:00'
summary:
    enabled: '0'
    format: short
taxonomy:
    tag:
        - zilkos
        - unixmail
    category:
        - apropos
---

## Mentions légales

Unixmail.fr est un service de communication au public en ligne édité à titre non professionnel au sens de l’[article 6, III, 2° de la loi 2004-575 du 21 juin 2004](http://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000801164&idArticle=LEGIARTI000006421546&dateTexte=&categorieLien=cid). En conformité avec cet article, l’éditeur de ce service a décidé de rester anonyme et est exempt d’une déclaration [CNIL](http://www.cnil.fr/).

===

Ce service est hébergé par OVH:

> SAS  au capital de 10 000 000 €
> RCS Roubaix – Tourcoing 424 761 419 00045
> Code APE 6202A
> N° TVA : FR 22 424 761 419
> Siège social : 2 rue Kellermann – 59100 Roubaix – France.

Pour toutes réclamations, je vous invite à contacter l’auteur et seul mainteneur de ce service via [cette page](http://unixmail.fr/contact).

Pour le nom de domaine, le nécessaire a été fait auprès de l’[AFNIC](http://www.afnic.fr), les informations sur le nom de domaine du service sont disponibles via l’outil whois de l’AFNIC que [vous pouvez consulter ici](http://www.afnic.fr/fr/produits-et-services/services/whois/).

## Licence du contenu

Tous les contenus présents sur le site sont, sauf mention contraire sous licence Creative Commons BY-SA:

>>>>>> <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International</a>.

Pour les codes sources, la licence est précisée sur chaque article. Si toutefois elle n’est pas précisée, c’est la [WTFPL](http://www.wtfpl.net/about/) qui s’applique (vous avez donc tous les droits sur le code).

## Portée des propos

Tous les contenus présents sur Unixmail.fr sont à la seule initiative de l’auteur. Les opinions exprimées ici n’engagent donc que moi, et notamment pas mon employeur présent ou mes employeurs passés.
